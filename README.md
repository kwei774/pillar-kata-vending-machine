Vending Machine Kata - Kevin's Submission
====================

In this exercise you will build the brains of a vending machine.  It will accept money, make change, maintain
inventory, and dispense products.  All the things that you might expect a vending machine to accomplish.

The point of this kata to to provide a larger than trivial exercise that can be used to practice TDD.  A significant
portion of the effort will be in determining what tests should be written and, more importantly, written next.

Features
========

Accept Coins
------------
  
_As a vendor_  
_I want a vending machine that accepts coins_  
_So that I can collect money from the customer_  

The vending machine will accept valid coins (nickels, dimes, and quarters) and reject invalid ones (pennies).  When a
valid coin is inserted the amount of the coin will be added to the current amount and the display will be updated.
When there are no coins inserted, the machine displays INSERT COIN.  Rejected coins are placed in the coin return.

NOTE: The temptation here will be to create Coin objects that know their value.  However, this is not how a real
  vending machine works.  Instead, it identifies coins by their weight and size and then assigns a value to what
  was inserted.  You will need to do something similar.  This can be simulated using strings, constants, enums,
  symbols, or something of that nature.

Select Product
--------------

_As a vendor_  
_I want customers to select products_  
_So that I can give them an incentive to put money in the machine_  

There are three products: cola for $1.00, chips for $0.50, and candy for $0.65.  When the respective button is pressed
and enough money has been inserted, the product is dispensed and the machine displays THANK YOU.  If the display is
checked again, it will display INSERT COIN and the current amount will be set to $0.00.  If there is not enough money
inserted then the machine displays PRICE and the price of the item and subsequent checks of the display will display
either INSERT COIN or the current amount as appropriate.

Make Change
-----------

_As a vendor_  
_I want customers to receive correct change_  
_So that they will use the vending machine again_  

When a product is selected that costs less than the amount of money in the machine, then the remaining amount is placed
in the coin return.

Return Coins
------------

_As a customer_  
_I want to have my money returned_  
_So that I can change my mind about buying stuff from the vending machine_  

When the return coins button is pressed, the money the customer has placed in the machine is returned and the display shows
INSERT COIN.

Sold Out
--------

_As a customer_  
_I want to be told when the item I have selected is not available_  
_So that I can select another item_  

When the item selected by the customer is out of stock, the machine displays SOLD OUT.  If the display is checked again,
it will display the amount of money remaining in the machine or INSERT COIN if there is no money in the machine.

Exact Change Only
-----------------

_As a customer_  
_I want to be told when exact change is required_  
_So that I can determine if I can buy something with the money I have before inserting it_  

When the machine is not able to make change with the money in the machine for any of the items that it sells, it will
display EXACT CHANGE ONLY instead of INSERT COIN.


Assumptions
========

* Sold out message will override price information
* When machine returns change, it will print the number and value of coins returned
* When an item is purchased:
	* Machine will first prioritize using coins that add to exact change
		* Insert 3 quarters, 7 dimes, 13 nickels to buy candy (0.65)
		* Machine will use 2 quarters, 1 dime, and 1 nickel
		* Machine will return 1 quarter, 6 dime, 12 nickels
	* Machine will secondly prioritize using coins of larger denominations
		* Insert 2 quarters, 5 dimes, 10 nickels to buy chips (0.50)
		* Machine will use 2 quarters
		* Machine will return 5 dimes, 10 nickels
	* Machine will prioritize returning coins the customer inserted into it
		* Prevents customer from abusing the machine to get different coin denominations
		* Insert 1 quarter, 10 dimes, 20 nickels to buy chips (0.50)
		* Machine will return 8 dimes, 19 nickels
		* Machine will NOT return 7 quarters
	* Machine will make change using largest coin denominations
		* Insert 3 quarters to purchase cand
		* Machine will return 1 dime over 2 nickels


How to interact with vending machine
========

Use nearest coin diameter in mm  
Use nearest coin mass in g  

[ https://www.usmint.gov/learn/coin-and-medal-programs/coin-specifications ](https://www.usmint.gov/learn/coin-and-medal-programs/coin-specifications)

Will accept:  
Nickels (diameter of 21 mm, mass of 5 g)  
Dimes (diameter of 18 mm, mass of 2 g)  
Quarters (diameter of 24 mm, mass of 6 g)

Will reject anything else:  
example penny (diameter of 19 mm, mass of 3 g)  

* takes input from standard input
	* 1 to show the display
	* 2 to insert a coin
		* Enter coin diameter and mass
		* If coin diameter or mass is invalid, machine will print that it returned the coin
	* 3 to select an item
		* 1 to select soda
		* 2 to select chips
		* 3 to select candy
			* if there is change, machine return it using return coin rules
	* 4 to return coins
		* vending machine will display the number and value of coins returned
	* 5 to exit program


Build Information
========

Tested on Ubuntu Linux 16.04 LTS  
g++ 5.4.0  
cmake 3.5.1  
git 2.7.4

Building on ubuntu
```
mkdir build
cd build
cmake ..
make
```

to run all tests
```
make test
```


to run the vending machine
```
./VendingMachine
```
