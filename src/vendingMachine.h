#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include "vmItem.h"
#include "coin.h"
#include "coinHandler.h"
using namespace std;

#ifndef VENDINGMACHINE_H
#define VENDINGMACHINE_H

namespace vendingMachine{


class vendingMachine
{
    enum vendingMachineState{
            initialState
        ,   enterCoinDiameterState
        ,   enterCoinMassState
        ,   selectItemState
    };

    vmItem cola, chips, candy;
    coin nickel, dime, quarter;
    coinHandler nickelHandler, dimeHandler, quarterHandler;
    vector <coinHandler *> changeHandler;

    int prevDiameter;
    vendingMachineState currentState, nextState;
    int inputCommand;

    string runStates();
    string runInitialState();
    string runCoinDiameterState();
    string runCoinMassState();
    string runSelectItemState();

    bool canMakeChange();
    string getDisplay();

    int calculateInputCents();
    string buildMoneyString(int cents);

    coinHandler * findMatchingCoinHandler(const coin &inputCoin);

    bool canPurchaseItem(vmItem &item);
    string selectItem(vmItem &item);
    void purchaseItem(vmItem &item);
    void tryExactChange(int &cents);
    void useGreedyAmount(int &cents);
    void returnCoinsIfUsedTooMany(int &cents);

    bool canReturnCoins();
    string returnCoins();
    string returnCoinString();
    void resetInsertedCoins();
    
public:

    vendingMachine();
    ~vendingMachine();

    string runMachine(string input);

    void setNickelsInMachine(int num);
    void setDimesInMachine(int num);
    void setQuartersInMachine(int num);
    void setColaStock(int stock);
    void setChipStock(int stock);
    void setCandyStock(int stock);

    int getNickelsInMachine() const;
    int getDimesInMachine() const;
    int getQuartersInMachine() const;
    int getColaStock() const;
    int getChipStock() const;
    int getCandyStock() const;

};

}



#endif