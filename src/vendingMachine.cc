#include "vendingMachine.h"

namespace vendingMachine{

vendingMachine::vendingMachine()
    :   cola(100, 5)
    ,   chips(50, 5)
    ,   candy(65, 5)
    ,   nickel(21, 5)
    ,   dime(18, 2)
    ,   quarter(24, 6)
    ,   nickelHandler(nickel, 5, 5, 0)
    ,   dimeHandler(dime, 10, 5, 0)
    ,   quarterHandler(quarter, 25, 5, 0)
    ,   prevDiameter(0)
    ,   currentState(initialState)
    ,   nextState(initialState)
    ,   inputCommand(0)
{
    changeHandler = {&dimeHandler, &quarterHandler, &nickelHandler};
    sort(changeHandler.begin(), changeHandler.end(), sortCoinHandlerPointersByGreatestValue);
}

vendingMachine::~vendingMachine(){}

string vendingMachine::runMachine(string inputStr){
    stringstream parseInput;
    parseInput << inputStr;
    parseInput >> inputCommand;
    currentState = nextState;
    return runStates();
}

string vendingMachine::runStates(){
    switch(currentState){
        case initialState:
            return runInitialState();
        case enterCoinDiameterState:
            return runCoinDiameterState();
        case enterCoinMassState:
            return runCoinMassState();
        case selectItemState:
            return runSelectItemState();
        default:
            return "";
    }
}

string vendingMachine::runInitialState(){
    stringstream output;
    switch(inputCommand){
        case 1:
            output << getDisplay();
            nextState = initialState;
            break;
        case 2:
            output << "Enter diameter of inserted coin in mm: ";
            nextState = enterCoinDiameterState;
            break;
        case 3:
            output << "Enter 1 to select cola, 2 to select chips, 3 to select candy\n";
            nextState = selectItemState;
            break;
        case 4:
            output << returnCoins();
            nextState = initialState;
            break;
        case 5:
            output << "EXIT";
        default:
            nextState = initialState;
            break;
    }
    return output.str();
}

string vendingMachine::runCoinDiameterState(){
    prevDiameter = inputCommand;
    nextState = enterCoinMassState;
    return "Enter mass of inserted coin in g: ";
}

string vendingMachine::runCoinMassState(){
    stringstream output;
    coin possibleCoin(prevDiameter, inputCommand);
    coinHandler *matchingCoinHandler = findMatchingCoinHandler(possibleCoin);
    if (matchingCoinHandler != NULL){
        matchingCoinHandler->insertCoin();
    }
    else{
        output << "Returned coin of diameter ";
        output << prevDiameter;
        output << "mm and mass ";
        output << inputCommand;
        output << "g.\n";
    }
    nextState = initialState;
    return output.str();
}

string vendingMachine::runSelectItemState(){
    nextState = initialState;
    switch(inputCommand){
        case 1:
            return selectItem(cola);
        case 2:
            return selectItem(chips);
        case 3:
            return selectItem(candy);
        default:
            return "";
    }
}

int vendingMachine::calculateInputCents(){
    int sum = 0;
    for (auto specificCoinHandler : changeHandler){
        sum += specificCoinHandler->getInsertedCentsAmount();
    }
    return sum;
}

bool vendingMachine::canMakeChange(){
    if (nickelHandler.getCoinsInMachine() >= 2)
        return true;
    if (dimeHandler.getCoinsInMachine() >= 1 && nickelHandler.getCoinsInMachine() >= 1)
        return true;
    else 
        return false;
}

string vendingMachine::buildMoneyString(int cents){
    stringstream moneyString;
    moneyString << "$";
    moneyString << cents/100;
    moneyString << ".";
    moneyString << setw(2) << setfill('0') << cents % 100;
    moneyString << "\n";
    return moneyString.str();
}

string vendingMachine::getDisplay(){
    int currentCents = calculateInputCents();
    if (!canMakeChange()){
        return "EXACT CHANGE\n";
    }
    else if (currentCents == 0){
        return "INSERT COIN\n";
    }
    else if (currentCents > 0){
        return buildMoneyString(currentCents);
    }
    return "";
}

coinHandler * vendingMachine::findMatchingCoinHandler(const coin &inputCoin){
    for (auto specificCoinHandler : changeHandler){
        if (specificCoinHandler->matchesDimensions(inputCoin)){
            return specificCoinHandler;
        }
    }
    return NULL;
}

string vendingMachine::returnCoins(){
    string output;
    if (canReturnCoins()){
        output = returnCoinString();
        resetInsertedCoins();
    }
    return output;
}

bool vendingMachine::canReturnCoins(){
    return calculateInputCents() != 0;
}

string vendingMachine::returnCoinString(){
    stringstream output;
    for (auto specificCoinHandler : changeHandler){
        if (specificCoinHandler->getInsertedCoins() != 0){
            output << "Returned ";
            output << specificCoinHandler->getInsertedCoins();
            output << " coin/s of value ";
            output << specificCoinHandler->getValue();
            output << ".\n";
        }
    }
    return output.str();
}

void vendingMachine::resetInsertedCoins(){
    for (auto specificCoinHandler : changeHandler){
        specificCoinHandler->returnInsertedCoins();
    }
}

void vendingMachine::tryExactChange(int &cents){
    for(auto specificCoinHandler : changeHandler){
        specificCoinHandler->useIdealCoinAmount(cents);
    }
    // for_each(
    //     changeHandler.begin(), 
    //     changeHandler.end(), 
    //     [&centsToDeduct](coinHandler * specificCoinHandler) {
    //         specificCoinHandler->useIdealCoinAmount(cents);
    //     });
}

void vendingMachine::useGreedyAmount(int &cents){
    for(auto specificCoinHandler : changeHandler){
        specificCoinHandler->useGreedyCoinAmount(cents);
    }
}

void vendingMachine::returnCoinsIfUsedTooMany(int &cents){
    for(auto specificCoinHandler : changeHandler){
        specificCoinHandler->returnCoinChange(cents);
    }
}

void vendingMachine::purchaseItem(vmItem &item){
    item.buyItem();
    int centsToDeduct = item.getCostInCents();
    tryExactChange(centsToDeduct);
    useGreedyAmount(centsToDeduct);
    returnCoinsIfUsedTooMany(centsToDeduct);
}

bool vendingMachine::canPurchaseItem(vmItem &item){
    return ( item.inStock()
            && ( calculateInputCents() >= item.getCostInCents() )
            );
}

string vendingMachine::selectItem(vmItem &item){
    stringstream output;
    if (canPurchaseItem(item)){
        output << "THANK YOU\n";
        purchaseItem(item);
        output << returnCoins();
    }
    else if (!item.inStock()){
        output << "SOLD OUT\n";
    }
    else{
        output << "PRICE ";
        output << buildMoneyString(item.getCostInCents());
    }
    return output.str();
}

void vendingMachine::setNickelsInMachine(int num){
    nickelHandler.setCoinsInMachine(num);
}

void vendingMachine::setDimesInMachine(int num){
    dimeHandler.setCoinsInMachine(num);
}

void vendingMachine::setQuartersInMachine(int num){
    quarterHandler.setCoinsInMachine(num);
}

void vendingMachine::setColaStock(int stock){
    cola.setStockNum(stock);
}

void vendingMachine::setChipStock(int stock){
    chips.setStockNum(stock);
}

void vendingMachine::setCandyStock(int stock){
    candy.setStockNum(stock);
}

int vendingMachine::getNickelsInMachine() const{
    return nickelHandler.getCoinsInMachine();
}

int vendingMachine::getDimesInMachine() const{
    return dimeHandler.getCoinsInMachine();
}

int vendingMachine::getQuartersInMachine() const{
    return quarterHandler.getCoinsInMachine();
}

int vendingMachine::getColaStock() const{
    return cola.getStockNum();
}

int vendingMachine::getChipStock() const{
    return chips.getStockNum();
}

int vendingMachine::getCandyStock() const{
    return candy.getStockNum();
}


} //namespace


