#include "coinHandler.h"
using namespace std;

namespace vendingMachine{

coinHandler::coinHandler()
    : coinHandler(0, 0, 0, 0, 0)
{}

coinHandler::coinHandler(
        int diameter
    ,   int mass
    ,   int valueIn
    ,   int coinsInMachineIn
    ,   int insertedCoinsIn
    )

    :   coinInfo(diameter, mass)
    ,   coinValue(valueIn)
    ,   coinsInMachine(coinsInMachineIn)
    ,   insertedCoins(insertedCoinsIn)
{}
coinHandler::coinHandler(
        coin coinIn
    ,   int valueIn
    ,   int coinsInMachineIn
    ,   int insertedCoinsIn
    )
    :   coinInfo(coinIn)
    ,   coinValue(valueIn)
    ,   coinsInMachine(coinsInMachineIn)
    ,   insertedCoins(insertedCoinsIn)
{}

coinHandler::~coinHandler(){}

int coinHandler::returnInsertedCoins(){
    int coinsReturned = insertedCoins;
    insertedCoins = 0;
    return coinsReturned;
}

void coinHandler::useCoins(int &cents, int coinsToUse){
    insertedCoins -= coinsToUse;
    coinsInMachine += coinsToUse;
    cents -= (coinValue * coinsToUse);
}

void coinHandler::useIdealCoinAmount(int &cents){
    int coinsToUse = min(insertedCoins, cents / coinValue);
    useCoins(cents, coinsToUse);
}

void coinHandler::useGreedyCoinAmount(int &cents){
    int coinsToUse = min(insertedCoins, (int) ceil( (double) cents / (double) coinValue));
    if (coinsToUse > 0)
        useCoins(cents, coinsToUse);
}

void coinHandler::returnCoinChange(int &cents){
    int coinsToReturn = min(coinsInMachine, -1 * (int) ceil( (double) cents / (double) coinValue));
    useCoins(cents, -1 * coinsToReturn);
}

bool coinHandler::matchesDimensions(const coin &otherCoin){
    return (coinInfo == otherCoin);
}

void coinHandler::insertCoin(){
    insertedCoins++;
}

bool coinHandler::operator < (const coinHandler &other) const{
    return coinValue < other.getValue();
}

bool coinHandler::operator == (const coinHandler &other) const{
    return (
            coinInfo.diameter == other.getDiameter()
        &&  coinInfo.mass == other.getMass()
        &&  coinValue == other.getValue()
        &&  coinsInMachine == other.getCoinsInMachine()
        &&  insertedCoins == other.getInsertedCoins()
        );
}

int coinHandler::getDiameter() const{
    return coinInfo.diameter;
}

int coinHandler::getMass() const{
    return coinInfo.mass;
}

int coinHandler::getValue() const{
    return coinValue;
}

int coinHandler::getCoinsInMachine() const{
    return coinsInMachine;
}

int coinHandler::getInsertedCoins() const{
    return insertedCoins;
}

int coinHandler::getInsertedCentsAmount() const{
    return coinValue * insertedCoins;
}

void coinHandler::setCoinsInMachine(int value){
    coinsInMachine = value;
}

bool sortCoinHandlerPointers(const coinHandler * first, const coinHandler * second){
    return *first < *second;
}

bool sortCoinHandlerPointersByGreatestValue(const coinHandler * first, const coinHandler * second){
    return ( first->getValue() > second->getValue() );
}

} //namespace