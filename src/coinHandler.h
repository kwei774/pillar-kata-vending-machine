#ifndef COINHANDLER_H
#define COINHANDLER_H

#include "coin.h"
#include <string>
#include <sstream>
#include <cmath>
#include <algorithm>
using namespace std;

namespace vendingMachine{

class coinHandler{
    coin coinInfo;
    int coinValue;
    int coinsInMachine;
    int insertedCoins;

    void useCoins(int &cents, int coinsToUse);

public:
    coinHandler();
    coinHandler(int diameter, int mass, int valueIn, int coinsInMachineIn, int insertedCoinsIn);
    coinHandler(coin coinIn, int valueIn, int coinsInMachineIn, int insertedCoinsIn);
    ~coinHandler();

    int returnInsertedCoins();
    void useIdealCoinAmount(int &cents);
    void useGreedyCoinAmount(int &cents);
    void returnCoinChange(int &cents);

    void insertCoin();

    bool matchesDimensions(const coin &c);

    bool operator < (const coinHandler &other) const;
    bool operator == (const coinHandler &other) const;

    int getDiameter() const;
    int getMass() const;
    int getValue() const;
    int getCoinsInMachine() const;
    int getInsertedCoins() const;
    int getInsertedCentsAmount() const;

    void setCoinsInMachine(int value);


};
bool sortCoinHandlerPointers(const coinHandler * first, const coinHandler * second);
bool sortCoinHandlerPointersByGreatestValue(const coinHandler * first, const coinHandler * second);

}//namespace


#endif