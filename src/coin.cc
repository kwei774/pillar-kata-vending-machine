#include "coin.h"

namespace vendingMachine
{

coin::coin()
    :   coin(0, 0){}

coin::coin(int diameterIn, int massIn)
    :   diameter(diameterIn)
    ,   mass(massIn)
    {}

coin::~coin(){}

bool coin::operator==(const coin &other) const{
    return (this->diameter == other.diameter && this->mass == other.mass);
}

bool coin::operator!=(const coin &other) const{
    return !(*this == other);
}

coin & coin::operator = (const coin &other){
    this->diameter = other.diameter;
    this->mass = other.mass;
    return *this;
}

} //namespace