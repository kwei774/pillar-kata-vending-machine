#include <iostream>
#include "vendingMachine.h"

using namespace std;



int main(){

    vendingMachine::vendingMachine vm;
    string input, output;

    cout << "Input 1 to show vending machine display\n";
    cout << "Input 2 to insert coin\n";
    cout << "Input 3 to select item\n";
    cout << "Input 4 to return item\n";
    cout << "Input 5 to exit\n";

    while(1){
        cin >> input;
        output = vm.runMachine(input);
        if (output == "EXIT"){
            break;
        }
        cout << output;
    }
    return 0;
}
