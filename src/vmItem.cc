#include "vmItem.h"

namespace vendingMachine{

vmItem::vmItem()
    :   vmItem(0, 0){}

vmItem::vmItem(int cost, int stock)
    :   costInCents(cost)
    ,   stockNum(stock){}

vmItem::~vmItem(){}

bool vmItem::inStock() const{ 
    return (stockNum > 0);
}

int vmItem::getCostInCents() const{
    return costInCents;
}

int vmItem::getStockNum() const{
    return stockNum;
}

void vmItem::setStockNum(int num){
    stockNum = num;
}

void vmItem::buyItem(){
    stockNum--;
}

}//namespace