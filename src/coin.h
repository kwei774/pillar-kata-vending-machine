#ifndef COIN_H
#define COIN_H

namespace vendingMachine
{

class coin
{
public:
    coin();
    coin(int diameterIn, int massIn);
    ~coin();
    int diameter;
    int mass;

    bool operator == (const coin &other) const;
    bool operator != (const coin &other) const;
    coin & operator = (const coin &other);
};

} //namespace

#endif