#ifndef VMITEM_H
#define VMITEM_H

namespace vendingMachine
{

class vmItem{
    int costInCents;
    int stockNum;
public:
    vmItem();
    vmItem(int cost, int stock);
    ~vmItem();
    bool inStock() const;
    int getCostInCents() const;
    int getStockNum() const;
    void setStockNum(int num);
    void buyItem();
};

} //namespace

#endif