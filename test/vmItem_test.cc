#include "gtest/gtest.h"
#include "vmItem.h"

namespace vendingMachine
{

// The fixture for testing class Foo.
class vmItemTest : public ::testing::Test {

};

TEST(vmItemTest, defaultCtor){
    vmItem item;
    EXPECT_EQ(0, item.getCostInCents());
    EXPECT_EQ(0, item.getStockNum());
}

TEST(vmItemTest, ctorInit){
    vmItem item(100, 5);
    EXPECT_EQ(100, item.getCostInCents());
    EXPECT_EQ(5, item.getStockNum());
}


TEST(vmItemTest, setValues){
    vmItem item;
    item.setStockNum(50);
    EXPECT_EQ(50, item.getStockNum());
}

TEST(vmItemTest, itemIsNotInStock){
    vmItem item;
    EXPECT_EQ(false, item.inStock());
}

TEST(vmItemTest, itemIsInStock){
    vmItem item(0, 5);
    EXPECT_EQ(true, item.inStock());
}

TEST(vmItemTest, buyItemDecreasesStock){
    vmItem item(0, 5);
    item.buyItem();
    EXPECT_EQ(4, item.getStockNum());
}

}//namespace


GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

