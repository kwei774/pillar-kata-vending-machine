
#include "gtest/gtest.h"
#include "vendingMachine.h"

namespace vendingMachine
{

// The fixture for testing class Foo.
class vendingMachineTest : public ::testing::Test {
protected:
// You can remove any or all of the following functions if its body
// is empty.

    vendingMachineTest() {
    // You can do set-up work for each test here.
    }

    virtual ~vendingMachineTest() {
    // You can do clean-up work that doesn't throw exceptions here.
    }

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
    }

    virtual void TearDown() {
    // Code here will be called immediately after each test (rightd
    // before the destructor).
    }

    // Objects declared here can be used by all tests in the test case for Foo.
    vendingMachine vm;
    string expectedOutput;
    int quartersInMachine = vm.getQuartersInMachine();
    int dimesInMachine = vm.getDimesInMachine();
    int nickelsInMachine = vm.getNickelsInMachine();

    void insertCoin(coin c, int num, string output){
        for (int coinsInserted = 0; coinsInserted < num; coinsInserted++){
            EXPECT_EQ("Enter diameter of inserted coin in mm: ", vm.runMachine("2"));
            EXPECT_EQ("Enter mass of inserted coin in g: ", vm.runMachine(to_string(c.diameter)));
            EXPECT_EQ(output, vm.runMachine(to_string(c.mass)));
        }
    }

    void insertNickel(int num){
        coin nickel(21, 5);
        insertCoin(nickel, num, "");
    }

    void insertDime(int num){
        coin dime(18, 2);
        insertCoin(dime, num, "");
    }

    void insertQuarter(int num){
        coin quarter(24, 6);
        insertCoin(quarter, num, "");
    }

    void selectItem(string output, string item){
        EXPECT_EQ("Enter 1 to select cola, 2 to select chips, 3 to select candy\n", vm.runMachine("3"));
        EXPECT_EQ(output, vm.runMachine(item));
    }

    void selectCola(string output){
        selectItem(output, "1");
    }

    void selectChips(string output){
        selectItem(output, "2");
    }

    void selectCandy(string output){
        selectItem(output, "3");
    }

    void expectInsertCoinMessage(){
        EXPECT_EQ("INSERT COIN\n", vm.runMachine("1"));
    }
    void expectExactChangeMessage(){
        EXPECT_EQ("EXACT CHANGE\n", vm.runMachine("1"));
    }

};

TEST_F(vendingMachineTest, defaultConstructor){
}

TEST_F(vendingMachineTest, defaultgetDisplay){
    expectInsertCoinMessage();
}

//Insert coin feature

TEST_F(vendingMachineTest, whenANickelIsInsertedMachineShouldUpdateMoney){
    insertNickel(1);
    EXPECT_EQ("$0.05\n", vm.runMachine("1"));
}

TEST_F(vendingMachineTest, whenADimeIsInsertedMachineShouldUpdateMoney){
    insertDime(1);
    EXPECT_EQ("$0.10\n", vm.runMachine("1"));
}

TEST_F(vendingMachineTest, whenAQuarterIsInsertedMachineShouldUpdateMoney){
    insertQuarter(1);
    EXPECT_EQ("$0.25\n", vm.runMachine("1"));
}

TEST_F(vendingMachineTest, whenInvalidDiameterAndMassCoinsAreInsertedMachineWillReject){
    coin c(19, 3);
    insertCoin(c, 1, "Returned coin of diameter 19mm and mass 3g.\n");
    expectInsertCoinMessage();
}

TEST_F(vendingMachineTest, whenInvalidDiameterCoinsAreInsertedMachineWillReject){
    coin c(20, 6);
    insertCoin(c, 1, "Returned coin of diameter 20mm and mass 6g.\n");
    expectInsertCoinMessage();
}

TEST_F(vendingMachineTest, whenInvalidMassCoinsAreInsertedMachineWillReject){
    coin c(21, 10);
    insertCoin(c, 1, "Returned coin of diameter 21mm and mass 10g.\n");
    expectInsertCoinMessage();
}

//select product feature

TEST_F(vendingMachineTest, selectingColaWithoutEnoughMoneyShouldDisplayPrice){
    selectCola("PRICE $1.00\n");
}

TEST_F(vendingMachineTest, selectingColaWithExactChangeInQuartersShouldReturnThankYou){
    insertQuarter(4);
    selectCola("THANK YOU\n");
}

TEST_F(vendingMachineTest, buyingColaWithQuartersShouldIncreaseQuartersInMachine){
    insertQuarter(4);
    selectCola("THANK YOU\n");
    EXPECT_EQ(quartersInMachine + 4, vm.getQuartersInMachine());

}

TEST_F(vendingMachineTest, buyingColaShouldDecreaseColaStock){
    vm.setColaStock(1);
    insertQuarter(4);
    selectCola("THANK YOU\n");
    EXPECT_EQ(0, vm.getColaStock());
}

TEST_F(vendingMachineTest, selectingChipsWithExactChangeShouldReturnThankYou){
    insertQuarter(2);
    selectChips("THANK YOU\n");
}

TEST_F(vendingMachineTest, selectingChipsWithoutEnoughMoneyShouldDisplayPrice){
    selectChips("PRICE $0.50\n");
}

TEST_F(vendingMachineTest, buyingChipsShouldDecreaseChipStock){
    int startingChipStock = vm.getChipStock();
    insertQuarter(2);
    selectChips("THANK YOU\n");
    EXPECT_EQ(startingChipStock - 1, vm.getChipStock());
}

TEST_F(vendingMachineTest, buyingChipsWithQuartersShouldIncreaseQuartersInMachine){
    insertQuarter(2);
    selectChips("THANK YOU\n");
    EXPECT_EQ(quartersInMachine + 2, vm.getQuartersInMachine());
}

TEST_F(vendingMachineTest, buyingChipsWithDimessShouldIncreaseDimesInMachine){
    insertDime(5);
    selectChips("THANK YOU\n");
    EXPECT_EQ(dimesInMachine + 5, vm.getDimesInMachine());
}

TEST_F(vendingMachineTest, buyingChipsWithNickelsShouldIncreaseNickelsInMachine){
    insertNickel(10);
    selectChips("THANK YOU\n");
    EXPECT_EQ(nickelsInMachine + 10, vm.getNickelsInMachine());
}

TEST_F(vendingMachineTest, buyingChipsWithManyCoinsShouldPrioritizeExactChangeAndLargestCoins){
    insertQuarter(1);
    insertDime(10);
    insertNickel(20);
    expectedOutput += "THANK YOU\n";
    expectedOutput += "Returned 8 coin/s of value 10.\n";
    expectedOutput += "Returned 19 coin/s of value 5.\n";
    selectChips(expectedOutput);
    EXPECT_EQ(quartersInMachine + 1, vm.getQuartersInMachine());
    EXPECT_EQ(dimesInMachine + 2, vm.getDimesInMachine());
    EXPECT_EQ(nickelsInMachine + 1, vm.getNickelsInMachine());
}

TEST_F(vendingMachineTest, selectingCandyWithoutEnoughMoneyShouldDisplayPrice){
    selectCandy("PRICE $0.65\n");
}

TEST_F(vendingMachineTest, selectingCandyWithExactChangeShouldReturnThankYou){
    insertQuarter(2);
    insertDime(1);
    insertNickel(1);
    selectCandy("THANK YOU\n");
}

TEST_F(vendingMachineTest, buyingCandyWithThreeQuartersShouldReturnADime){
    insertQuarter(3);
    expectedOutput += "THANK YOU\n";
    expectedOutput += "Returned 1 coin/s of value 10.\n";
    selectCandy(expectedOutput);
    EXPECT_EQ(quartersInMachine + 3, vm.getQuartersInMachine());
    EXPECT_EQ(dimesInMachine - 1, vm.getDimesInMachine());
}

TEST_F(vendingMachineTest, buyingCandyWithThreeQuartersShouldReturnTwoNickelsWhenThereAreNoDimes){
    vm.setDimesInMachine(0);
    vm.setCandyStock(100);
    insertQuarter(3);
    expectedOutput += "THANK YOU\n";
    expectedOutput += "Returned 2 coin/s of value 5.\n";
    selectCandy(expectedOutput);
    EXPECT_EQ(quartersInMachine + 3 , vm.getQuartersInMachine());
    EXPECT_EQ(nickelsInMachine - 2, vm.getNickelsInMachine());
}

TEST_F(vendingMachineTest, buyingCandyWithManyCoinsWillPrioritizeExactChange){
    insertQuarter(3);
    insertDime(7);
    insertNickel(13);

    expectedOutput += "THANK YOU\n";
    expectedOutput += "Returned 1 coin/s of value 25.\n";
    expectedOutput += "Returned 6 coin/s of value 10.\n";
    expectedOutput += "Returned 12 coin/s of value 5.\n";
    selectCandy(expectedOutput);

    EXPECT_EQ(quartersInMachine + 2, vm.getQuartersInMachine());
    EXPECT_EQ(dimesInMachine + 1, vm.getDimesInMachine());
    EXPECT_EQ(nickelsInMachine + 1, vm.getNickelsInMachine());
}

TEST_F(vendingMachineTest, setColaStockAndGetColaStock){
    vm.setColaStock(10);
    EXPECT_EQ(10, vm.getColaStock());
}

//return coin

TEST_F(vendingMachineTest, insertAndReturnOneQuarterShouldOutputMessageThenDisplayInsertCoin){
    insertQuarter(1);
    EXPECT_EQ("Returned 1 coin/s of value 25.\n", vm.runMachine("4"));
    expectInsertCoinMessage();
}

TEST_F(vendingMachineTest, insertAndReturnDimesShouldOutputMessage){
    insertDime(2);
    EXPECT_EQ("Returned 2 coin/s of value 10.\n", vm.runMachine("4"));
}

TEST_F(vendingMachineTest, insertAndReturnDifferentCoinsShouldOutputMessage){
    insertQuarter(1);
    insertDime(2);
    insertNickel(5);
    expectedOutput += "Returned 1 coin/s of value 25.\n";
    expectedOutput += "Returned 2 coin/s of value 10.\n";
    expectedOutput += "Returned 5 coin/s of value 5.\n";
    EXPECT_EQ( expectedOutput, vm.runMachine("4"));
}

TEST_F(vendingMachineTest, returningCoinsTwiceShouldNotGiveOutCoinsASecondTime){
    insertQuarter(1);
    insertDime(2);
    insertNickel(3);
    expectedOutput += "Returned 1 coin/s of value 25.\n";
    expectedOutput += "Returned 2 coin/s of value 10.\n";
    expectedOutput += "Returned 3 coin/s of value 5.\n";
    EXPECT_EQ( expectedOutput, vm.runMachine("4"));
    EXPECT_EQ( "", vm.runMachine("4"));
}

//sold out feature


TEST_F(vendingMachineTest, selectingChipsWhenOutOfStockShouldReturnSoldOut){
    vm.setChipStock(0);
    selectChips("SOLD OUT\n");
}

TEST_F(vendingMachineTest, selectingColaWhenOutOfStockShouldReturnSoldOut){
    vm.setColaStock(0);
    insertQuarter(4);
    selectCola("SOLD OUT\n");
}

TEST_F(vendingMachineTest, soldOutColaAndNotEnoughMoneyShouldReturnSoldOut){
    vm.setColaStock(0);
    insertQuarter(3);
    selectCola("SOLD OUT\n");
}

//exact change only
TEST_F(vendingMachineTest, whenMachineDoesNotHaveEnoughCoinsToMakeChangeItShouldDisplayExactChange){
    vm.setNickelsInMachine(0);
    vm.setDimesInMachine(0);
    vm.setQuartersInMachine(0);
    expectExactChangeMessage();
}

TEST_F(vendingMachineTest, whenMachineDoesNotHaveEnoughNickelsToMakeChangeItShouldDisplayExactChange){
    vm.setNickelsInMachine(0);
    vm.setDimesInMachine(10);
    vm.setQuartersInMachine(10);
    expectExactChangeMessage();
}

TEST_F(vendingMachineTest, whenMachineDoesNotHaveEnoughNickelAndDimesToMakeChangeItShouldDisplayExactChange){
    vm.setNickelsInMachine(1);
    vm.setDimesInMachine(0);
    vm.setQuartersInMachine(10);
    expectExactChangeMessage();
}

//I really hope we only need 10 cents

//Insert coin  / exact change threshold
TEST_F(vendingMachineTest, whenMachineCanMakeChangeWithTwoNickelsItShouldDisplayInsertCoin){
    vm.setNickelsInMachine(2);
    vm.setDimesInMachine(0);
    vm.setQuartersInMachine(0);
    expectInsertCoinMessage();
}

TEST_F(vendingMachineTest, whenMachineCanMakeChangeWithOneNickelAndOneDimeItShouldDisplayInsertCoin2){
    vm.setNickelsInMachine(1);
    vm.setDimesInMachine(1);
    vm.setQuartersInMachine(0);
    expectInsertCoinMessage();
}

TEST_F(vendingMachineTest, whenMachineReceivesInvalidInputItDoesNothingAndGoesBackToInitialState){

    expectInsertCoinMessage();
    EXPECT_EQ("", vm.runMachine("a"));
    expectInsertCoinMessage();

    EXPECT_EQ("Enter diameter of inserted coin in mm: ", vm.runMachine("2"));
    EXPECT_EQ("Enter mass of inserted coin in g: ", vm.runMachine("a"));
    EXPECT_EQ("Returned coin of diameter 0mm and mass 0g.\n", vm.runMachine("a"));

    expectInsertCoinMessage();

    EXPECT_EQ("Enter 1 to select cola, 2 to select chips, 3 to select candy\n", vm.runMachine("3"));
    EXPECT_EQ("", vm.runMachine("a"));
}

TEST_F(vendingMachineTest, whenFiveIsEnteredIntoMachineItExits){
    EXPECT_EQ("EXIT", vm.runMachine("5"));
}

} //namespace


GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
