#include "gtest/gtest.h"
#include "coinHandler.h"
#include <vector>
#include <algorithm>
using namespace std;

namespace vendingMachine
{

// The fixture for testing class Foo.
class coinHandlerTest : public ::testing::Test {

};

TEST(coinHandlerTest, defaultctor){
    coinHandler ch;
    EXPECT_EQ(0, ch.getDiameter());
    EXPECT_EQ(0, ch.getMass());
    EXPECT_EQ(0, ch.getValue());
    EXPECT_EQ(0, ch.getCoinsInMachine());
    EXPECT_EQ(0, ch.getInsertedCoins());
    EXPECT_EQ(0, ch.getInsertedCentsAmount());
}

TEST(coinHandlerTest, assignmentctor){
    coinHandler ch(5, 4, 100, 2, 5);
    EXPECT_EQ(5, ch.getDiameter());
    EXPECT_EQ(4, ch.getMass());
    EXPECT_EQ(100, ch.getValue());
    EXPECT_EQ(2, ch.getCoinsInMachine());
    EXPECT_EQ(5, ch.getInsertedCoins());
}

TEST(coinHandlerTest, assignmentctorwithCoin){
    coin c(100, 5);
    coinHandler ch(c, 100, 2, 5);
    EXPECT_EQ(100, ch.getDiameter());
    EXPECT_EQ(5, ch.getMass());
    EXPECT_EQ(100, ch.getValue());
    EXPECT_EQ(2, ch.getCoinsInMachine());
    EXPECT_EQ(5, ch.getInsertedCoins());
}

TEST(coinHandlerTest, setCoinsInMachineWillChangeWhenSet){
    coinHandler ch;
    EXPECT_EQ(0, ch.getCoinsInMachine());
    ch.setCoinsInMachine(5);
    EXPECT_EQ(5, ch.getCoinsInMachine());
}

TEST(coinHandlerTest, getCoinsInMachineWillReturnTotalValueOfCoins){
    coinHandler ch(0, 0, 5, 0, 2);
    EXPECT_EQ(10, ch.getInsertedCentsAmount());
}

TEST(coinHandlerTest, useIdealCoinAmountWillOnlyUseUpToInputCentsOfCoins){
    coinHandler ch(0, 0, 5, 0, 100);
    EXPECT_EQ(5, ch.getValue());
    EXPECT_EQ(0, ch.getCoinsInMachine());
    EXPECT_EQ(100, ch.getInsertedCoins());
    int cents = 50;
    ch.useIdealCoinAmount(cents);
    EXPECT_EQ(10, ch.getCoinsInMachine());
    EXPECT_EQ(90, ch.getInsertedCoins());
}

TEST(coinHandlerTest, useGreedyCoinAmountUseFloorOfCoinsNeededToReachZeroCents){
    coinHandler ch(0, 0, 50, 0, 100);
    EXPECT_EQ(50, ch.getValue());
    EXPECT_EQ(0, ch.getCoinsInMachine());
    EXPECT_EQ(100, ch.getInsertedCoins());
    int cents = 75;
    ch.useIdealCoinAmount(cents);
    EXPECT_EQ(1, ch.getCoinsInMachine());
    EXPECT_EQ(99, ch.getInsertedCoins());
}

TEST(coinHandlerTest, useGreedyCoinAmountUseCeilingOfCoinsNeededToReachZeroCents){
    coinHandler ch(0, 0, 50, 0, 100);
    EXPECT_EQ(50, ch.getValue());
    EXPECT_EQ(0, ch.getCoinsInMachine());
    EXPECT_EQ(100, ch.getInsertedCoins());
    int cents = 75;
    ch.useGreedyCoinAmount(cents);
    EXPECT_EQ(2, ch.getCoinsInMachine());
    EXPECT_EQ(98, ch.getInsertedCoins());
}

TEST(coinHandlerTest, returnCoinChangeWillPutCoinsBackIntoInsertedCoins){
    coinHandler ch(0, 0, 50, 100, 100);
    EXPECT_EQ(50, ch.getValue());
    EXPECT_EQ(100, ch.getCoinsInMachine());
    EXPECT_EQ(100, ch.getInsertedCoins());
    int cents = -75;
    ch.returnCoinChange(cents);
    EXPECT_EQ(99, ch.getCoinsInMachine());
    EXPECT_EQ(101, ch.getInsertedCoins());
}

TEST(coinHandlerTest, returnInsertedCoinsWillReturnCoinsInsertedAndChangeInsertedCoinsToZero){
    coinHandler ch(0, 0, 0, 0, 100);
    EXPECT_EQ(100, ch.getInsertedCoins());
    EXPECT_EQ(100, ch.returnInsertedCoins());
    EXPECT_EQ(0, ch.getInsertedCoins());
}

TEST(coinHandlerTest, sameCoinMassAndDiameterWillReturnTrueforMatchesDimensions){
    coin c(10, 5);
    coinHandler ch(c, 0, 0, 100);
    EXPECT_EQ(true, ch.matchesDimensions(c));
}

TEST(coinHandlerTest, differentCoinMassAndDiameterWillReturnFalseforMatchesDimensions){
    coin c(10, 5);
    coin differentCoin(100, 50);
    coinHandler ch(c, 0, 0, 100);
    EXPECT_EQ(false, ch.matchesDimensions(differentCoin));
}

TEST(coinHandlerTest, insertCoinRaisesInsertedCoins){
    coinHandler ch(0, 0, 0, 0, 0);
    EXPECT_EQ(0, ch.getInsertedCoins());
    ch.insertCoin();
    EXPECT_EQ(1, ch.getInsertedCoins());
}

TEST(coinHandlerTest, coinHandlerIsSortedByValue){
    coinHandler quarterHandler(0, 0, 25, 0, 0);
    coinHandler twentyHandler(0, 0, 20, 0, 0);
    coinHandler dimeHandler(0, 0, 10, 0, 0);
    coinHandler nickelHandler(0, 0, 5, 0, 0);

    vector <coinHandler> changeHandler = {quarterHandler, nickelHandler, twentyHandler, dimeHandler};
    sort(changeHandler.begin(), changeHandler.end());
    EXPECT_EQ(nickelHandler, changeHandler[0]);
    EXPECT_EQ(dimeHandler, changeHandler[1]);
    EXPECT_EQ(twentyHandler, changeHandler[2]);
    EXPECT_EQ(quarterHandler, changeHandler[3]);
}

TEST(coinHandlerTest, vectorOfCoinHandlerPointersIsSortedBySmallestValue){
    coinHandler quarterHandler(0, 0, 25, 0, 0);
    coinHandler twentyHandler(20, 0, 20, 0, 0);
    coinHandler dimeHandler(50, 0, 10, 0, 0);
    coinHandler nickelHandler(100, 0, 5, 0, 0);

    vector <coinHandler *> changeHandler = {&dimeHandler, &quarterHandler, &nickelHandler, &twentyHandler};
    sort(changeHandler.begin(), changeHandler.end(), sortCoinHandlerPointers);
    EXPECT_EQ(nickelHandler, *changeHandler[0]);
    EXPECT_EQ(dimeHandler, *changeHandler[1]);
    EXPECT_EQ(twentyHandler, *changeHandler[2]);
    EXPECT_EQ(quarterHandler, *changeHandler[3]);

    quarterHandler.setCoinsInMachine(50);
    EXPECT_EQ(50, (*changeHandler[3]).getCoinsInMachine());
}

TEST(coinHandlerTest, vectorOfCoinHandlerPointersIsSortedByGreatestValue){
    coinHandler quarterHandler(0, 0, 25, 0, 0);
    coinHandler twentyHandler(20, 0, 20, 0, 0);
    coinHandler dimeHandler(50, 0, 10, 0, 0);
    coinHandler nickelHandler(100, 0, 5, 0, 0);

    vector <coinHandler *> changeHandler = {&dimeHandler, &quarterHandler, &nickelHandler, &twentyHandler};
    sort(changeHandler.begin(), changeHandler.end(), sortCoinHandlerPointersByGreatestValue);
    EXPECT_EQ(quarterHandler, *changeHandler[0]);
    EXPECT_EQ(twentyHandler, *changeHandler[1]);
    EXPECT_EQ(dimeHandler, *changeHandler[2]);
    EXPECT_EQ(nickelHandler, *changeHandler[3]);
}


}//namespace

GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}