#include "coin.h"
#include "gtest/gtest.h"

namespace vendingMachine
{

// The fixture for testing class Foo.
class coinTest : public ::testing::Test {

};

TEST_F(coinTest, defaultctor){
    coin c;
    ASSERT_EQ(0, c.diameter);
    ASSERT_EQ(0, c.mass);
}

TEST_F(coinTest, ctorInit){
    coin c(10, 5);
    ASSERT_EQ(10, c.diameter);
    ASSERT_EQ(5, c.mass);
}

TEST_F(coinTest, copyctor){
    coin c(10, 5);
    coin coinCopy(c);
    ASSERT_EQ(10, coinCopy.diameter);
    ASSERT_EQ(5, coinCopy.mass);
}

TEST_F(coinTest, setValues){
    coin c;
    c.diameter = 10;
    c.mass = 5;
    ASSERT_EQ(10, c.diameter);
    ASSERT_EQ(5, c.mass);
}

TEST_F(coinTest, equalityOperator){
    coin left, right;
    left.diameter = 10;
    left.mass = 5;
    right.diameter = 10;
    right.mass = 5;
    ASSERT_EQ(left, right);
}

TEST_F(coinTest, inequalityOperator){
    coin left, right;
    left.diameter = 10;
    left.mass = 5;
    right.diameter = 1;
    right.mass = 5;
    ASSERT_NE(left, right);
}

TEST_F(coinTest, assignmentOperator){
    coin left, right;
    left.diameter = 10;
    left.mass = 5;
    right = left;
    ASSERT_EQ(left, right);
}

} //namespace


GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
